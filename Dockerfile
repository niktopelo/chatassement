# Use an official Node.js as a parent image
FROM node:18-alpine3.17

# Set the working directory in the container to /app
WORKDIR /app

# Copy package.json and package-lock.json to the workdir
COPY package*.json ./

# Install all dependencies
RUN yarn install

# Copy the current directory contents (Your Vue.js app) into the container at /app
COPY . .

# Make port 9000 available to the world outside this container
EXPOSE 9000

# Run the application when the container launches
CMD ["yarn", "quasar","dev"]

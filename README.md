# chat
Front end chat build with vue js 

## Launch with docker 
```bash
docker compose up 
```

## Install the dependencies without docker
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
# or
yarn quasar dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```



### Build the app for production
```bash
quasar build
```



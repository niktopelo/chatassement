import Axios, { AxiosResponse } from 'axios';

export default class ApiHelper {
  static getBaseUrl(): string {
    return 'http://localhost:3000/';
  }

  static formatUrl(urlEnd: string): string {
    return this.getBaseUrl() + urlEnd;
  }

  static setHeader(token: string) :void {
    Axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  }

  static getUserByToken():Promise<AxiosResponse> {
    return Axios({
      method: 'get',
      url: ApiHelper.formatUrl('getUserByToken'),
    });
  }

  static getConversations():Promise<AxiosResponse> {
    return Axios({
      method: 'get',
      url: ApiHelper.formatUrl('getConversations'),
    });
  }

  static uploadFile(data: File):Promise<AxiosResponse> {
    // we need formData to send the file as binary
    const formData :FormData = new FormData();
    formData.append('file', data);
    return Axios({
      method: 'post',
      url: ApiHelper.formatUrl('upload'),
      data: formData,
    });
  }

  static login(data: object):Promise<AxiosResponse> {
    return Axios({
      method: 'post',
      url: ApiHelper.formatUrl('login'),
      data,
    });
  }
}

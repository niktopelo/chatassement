import BaseMessage from 'src/BaseMessage';

export default class AttachmentMessage extends BaseMessage {
  url: string;

  name: string;

  constructor(
    messageId: string,
    from: string,
    type: string,
    status: string,
    time: string,
    conversationId: string,
    url: string,
    name: string,
  ) {
    super(messageId, from, type, status, time, conversationId);
    this.url = url;
    this.name = name;
  }
}

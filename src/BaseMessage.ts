export default class BaseMessage {
  messageId: string;

  from: string;

  type: string;

  status: string;

  time: string;

  conversationId: string;

  constructor(
    messageId: string,
    from: string,
    type: string,
    status: string,
    time: string,
    conversationId: string,
  ) {
    this.messageId = messageId;
    this.from = from;
    this.type = type;
    this.status = status;
    this.time = time;
    this.conversationId = conversationId;
  }
}

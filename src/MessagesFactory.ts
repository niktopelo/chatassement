import AttachmentMessage from 'src/AttachmentMessage';
import TextMessage from 'src/TextMessage';
import { v4 as uuidv4 } from 'uuid';

export type Message = AttachmentMessage | TextMessage;

// simple factory to create the messages of different types
export class MessagesFactory {
  static createMessage(data : any) : Message {
    if (data.type === 'text') {
      return new TextMessage(
        uuidv4(),
        data.from,
        data.type,
        data.status,
        data.time,
        data.conversationId,
        data.message,
      );
    }
    return new AttachmentMessage(
      uuidv4(),
      data.from,
      data.type,
      data.status,
      data.time,
      data.conversationId,
      `http://localhost:3000/getFile/${data.name}`,
      data.name,
    );
  }
}

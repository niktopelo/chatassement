import BaseMessage from 'src/BaseMessage';

export default class TextMessage extends BaseMessage {
  message: string;

  constructor(
    messageId: string,
    from: string,
    type: string,
    status: string,
    time: string,
    conversationId: string,
    message: string,
  ) {
    super(messageId, from, type, status, time, conversationId);
    this.message = message;
  }
}

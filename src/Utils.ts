import {
  Loading,
  Notify,
  QSpinnerCube,
} from 'quasar';

export default class Utils {
  static formatDateTimeToLocal(date: Date | string): string {
    return new Date(date).toLocaleString();
  }

  static showLoader(message?: string | null): void {
    Loading.show({
      spinner: QSpinnerCube,
      spinnerColor: 'secondary',
      backgroundColor: 'primary',
      message: message || 'Please wait',
      messageColor: '#6A6A6A',
    });
  }

  static closeLoader(): void {
    Loading.hide();
  }
}

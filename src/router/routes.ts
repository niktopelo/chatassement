import { RouteRecordRaw } from 'vue-router';
import { Cookies } from 'quasar';
import Utils from 'src/Utils';
import ApiHelper from 'src/ApiHelper';
import { AxiosResponse } from 'axios';

const makeRoutes = ({ store }: { store: any }) => {
  async function authIsValid() : Promise<boolean> {
    // if the store contain the token the user is logged
    if (store.token !== '') {
      return true;
    }
    // in other cas we will see if we got a cookie with the token
    if (Cookies.has('auth-assessment-token')) {
      Utils.showLoader();
      const authToken :string = Cookies.get('auth-assessment-token');
      ApiHelper.setHeader(authToken);
      try {
        // we will try to log with the cookie token
        const res : AxiosResponse = await ApiHelper.getUserByToken();
        if (!res || !res.data) {
          Utils.closeLoader();
          return false;
        }
        // once we get logged we set the user in the store
        store.setUser(res.data);
        // we set the axios headers with the token from the api
        ApiHelper.setHeader(res.data.token);
        // we set the new cookie
        Cookies.set(
          'auth-assessment-token',
          res.data.token,
          {
            sameSite: 'Strict',
            expires: 14,
            secure: true,
          },
        );
        Utils.closeLoader();
        return true;
      } catch (error:any) {
        if (error.response && error.response.status === 401) {
          // in case if we are not authorized we remove the cookie and close the loader
          Cookies.remove('auth-assessment-token');
          Utils.closeLoader();
        } else {
          // Non-401 errors can be handled here
          Utils.closeLoader();
        }
        return false;
      }
    }
    return false;
  }

  const routes: RouteRecordRaw[] = [
    {
      path: '/',
      component: () => import('layouts/MainLayout.vue'),
      children: [{ path: '', component: () => import('pages/IndexPage.vue') }],
    },
    {
      path: '/chat',
      component: () => import('layouts/MainLayout.vue'),
      children: [{ path: '', component: () => import('pages/ChatPage.vue') }],
      // the guard to protect the page
      beforeEnter: (to, from, next) => {
        authIsValid().then((res) => {
          if (!res) {
            next('/');
          } else {
            next();
          }
        });
      },
    },
    {
      path: '/:catchAll(.*)*',
      component: () => import('pages/ErrorNotFound.vue'),
    },
  ];
  return routes;
};
export default makeRoutes;

import { defineStore } from 'pinia';

export const userStore = defineStore('user', {
  state: () => ({
    id: '',
    email: '',
    role: '',
    token: '',
    selectedConversationId: '',
  }),
  actions: {
    setUser(nvUser : { id: string, email :string, role :string, token : string}) :void {
      this.id = nvUser.id;
      this.email = nvUser.email;
      this.role = nvUser.role;
      this.token = nvUser.token;
    },
    logout() :void {
      this.email = '';
      this.id = '';
      this.role = '';
      this.token = '';
      this.selectedConversationId = '';
    },
    setSelectedConversation(conversationId : string) :void {
      this.selectedConversationId = conversationId;
    },
  },
});
